const form = document.querySelector('.form');
const clear = document.querySelector('.clear-local-storage');
const submit = document.querySelector('.form__submit');
const userInfo = document.querySelector('.user-info');
const userName = document.querySelector('.form__user-name');
const userSurname = document.querySelector('.form__user-surname');
const dataLocalStorage = JSON.parse(localStorage.getItem('userData'))

// Создаем обьект для хранения данных в Local Storage
const data = {
    name: '',
    surname: '',
}
// Убираем перезагрузку страницы при отправке формы по умолчанию
form.addEventListener('submit', (e) => e.preventDefault());

// Очищаем localStorage
clear.addEventListener('click', () => localStorage.clear());

// Проверяем Local Storage на наличие данных клиента
const initializeLocalStorage = function (){
    dataLocalStorage !== null ? showUserData(dataLocalStorage) : null
}();

// Создаем функцию которая принимает данные пользователя и записывает их в обьект 
const getUserData  = () => {
    data.name = userName.value
    data.surname = userSurname.value
    return data
}

// Создаем функцию которая отображает данные пользователья на странице
function showUserData(props){
    userInfo.append(`Hello ${props.name} ${props.surname}`);
    form.style.display = 'none'
}

// Создаем функцию которая отправляет данные в Local Storage и отображает полученные 
// данные на странице
const sendToLocalStorage = () => {
    const data = getUserData()
    localStorage.setItem('userData', JSON.stringify(data))
    showUserData(data);
}
// Добавляем данные в local storage по клику
submit.addEventListener('click', sendToLocalStorage);

